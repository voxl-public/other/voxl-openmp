# Building openmp for VOXL

openmp is distributed for VOXL through the opkg package repo:

voxl-packages.modalai.com

It is installed as part of voxl-suite and should not need to be compiled from source unless you are trying something custom.

Currently this builds openmp v10.0.1 since this is the newest version that builds with the 4.9 aarch64 cross compiler. It is used by the voxl build of opencv.



### Building the 64-bit IPK from source

```
git submodule init
git submodule update

# start docker
voxl-docker -i voxl-cross
```

inside docker:
```
./build.sh
./make_package.sh
```
